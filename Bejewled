#include <SFML/Graphics.hpp>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

using namespace std;

const int windowX = 512;
const int windowY = 512;
const float dotRad = 16.0;
const int numDots = 512 / 32;  // 16

const sf::Color dotColour[8] = { sf::Color::Red, sf::Color::White, sf::Color::Blue,
sf::Color::Green, sf::Color::Cyan, sf::Color::Magenta,
sf::Color::Yellow, sf::Color::Black };


class Dot
{
public:
	virtual void draw(sf::RenderWindow& w);

	sf::Vector2f pos;
	int type;

};


void Dot::draw(sf::RenderWindow& w)
{
	sf::CircleShape s;
	s.setRadius(dotRad);
	s.setOrigin(dotRad, dotRad);
	s.setFillColor(dotColour[type]);
	s.setPosition(pos);
	w.draw(s);
}

using Pattern = std::vector < std::vector<bool> > ;
using Patterns = std::vector < Pattern > ;

class Board
{
public:
	Dot dots[numDots + 4][numDots + 4];

	Board();
	virtual void update();
	virtual void draw(sf::RenderWindow& w);

private:
	Patterns patterns;
	std::vector<sf::Vector2i> indeciesToRemove;

	bool loopPattern(int i, int j, int patternNr);

	void loadPatterns(const std::string& fileName);
	void removeMatchedPatterns();
};




void Board::update()
{
	int patternMax = patterns.size();
	for (int i = 2; i < numDots + 2; i++) {
		for (int j = 2; j < numDots + 2; j++) {
			for (int nr = 0; nr < patternMax; nr++) {
				loopPattern(i, j, nr);
			}
		}
	}
	removeMatchedPatterns();
}




bool Board::loopPattern(int i, int j, int patternNr) {
	int prevType;
	std::vector<sf::Vector2i> tempToRemove;
	bool first = true;			// we need this to handle patterns that does not start top left.
	for (int y = 0; y < patterns[patternNr].size(); y++) {
		for (int x = 0; x < patterns[patternNr][0].size(); x++) {
			if (patterns[patternNr][y][x]) {
				if (first) {
					prevType = dots[i + y][j + x].type;
					first = false;
				}
				if (dots[i + y][j + x].type == prevType && dots[i + y][j + x].type != 7) {
					tempToRemove.push_back({ i + y, j + x });
				}
				else {
					return false;
				}
			}
		}
	}
	indeciesToRemove.insert(std::end(indeciesToRemove), std::begin(tempToRemove), std::end(tempToRemove));
	return true;
}




void Board::loadPatterns(const std::string& fileName) {
	std::ifstream file;
	file.open(fileName);
	if (!file.is_open()) {
		std::cout << "Error: Failed to open " << fileName << std::endl;
		return;
	}

	Pattern newPattern;
	std::string textline;
	int count = 0;
	while (std::getline(file, textline)) {
		if (textline[0] == 'P') {
			patterns.push_back(newPattern);
			newPattern.clear();
			continue;
		}

		std::vector<bool> newRow;
		for (int i = 0; i < textline.length(); ++i) {
			newRow.push_back((bool)(textline[i] - '0'));
		}
		newPattern.push_back(newRow);
	}
}



void Board::removeMatchedPatterns() {
	for (auto it : indeciesToRemove) {
		dots[it.x][it.y].type = 7;
	}
	for (int i = numDots + 2; i > 2; i--) {
		for (int j = 2; j < numDots + 2; j++) {
			if (dots[i][j].type == 7) {
				dots[i][j].type = dots[i - 1][j].type;
				dots[i - 1][j].type = 7;
			}
		}
	}
	for (int j = 2; j < numDots + 2; j++) {
		for (int i = 2; i < numDots + 2; i++) {
			if (dots[i][j].type == 7) {
				dots[i][j].type = rand() % 7;
			}
			else {
				continue;
			}
		}
	}
	indeciesToRemove.clear();
}



Board::Board()
{
	loadPatterns("patterns.txt");
	for (auto& it : patterns) {
		for (auto & it2 : it) {
			for (auto &it3 : it2) {
				std::cout << it3;
			}
			std::cout << std::endl;
		}
		std::cout << std::endl << std::endl;;
	}

	std::cout << "aaa";
	for (int y = 0; y < patterns[0].size(); y++) {
		std::cout << std::endl;
		for (int x = 0; x < patterns[0][0].size(); x++) {
			std::cout << patterns[0][y][x];
		}
	}


	// Initialize interior
	for (int i = 0; i < numDots; i++) {
		for (int j = 0; j < numDots; j++) {
			dots[i + 2][j + 2].type = rand() % 7;  // Black is never used in interior, hence % 7 not % 8
			dots[i + 2][j + 2].pos = sf::Vector2f(j * 2 * dotRad + dotRad, i * 2 * dotRad + dotRad);
		}
	}

	// Inititialize boundary
	for (int i = 0; i < numDots; i++) {
		dots[i][0].type = 7;
		dots[i][1].type = 7;
		dots[0][i].type = 7;
		dots[1][i].type = 7;
		dots[18][i].type = 7;
		dots[19][i].type = 7;
		dots[i][18].type = 7;
		dots[i][19].type = 7;
	}

}


void Board::draw(sf::RenderWindow& w)
{
	for (int i = 0; i < numDots; i++) {
		for (int j = 0; j < numDots; j++) {
			dots[i + 2][j + 2].draw(w);
		}
	}
}



int main()
{
	sf::RenderWindow window(sf::VideoMode(512, 512), "My window");

	int x;
	int y;
	bool pressed = false;

	Board board;

	while (window.isOpen())
	{

		sf::Event event;


		while (window.pollEvent(event))
		{

			if (event.type == sf::Event::Closed) {
				window.close();
				exit(0);
			}


			if (!pressed) {
				if (event.type == sf::Event::MouseButtonPressed) {
					if (event.mouseButton.button == sf::Mouse::Left) {
						x = event.mouseButton.x / (2 * dotRad);
						y = event.mouseButton.y / (2 * dotRad);
						pressed = true;
					}
				}
			}
			else {
				if (event.type == sf::Event::MouseButtonPressed) {
					if (event.mouseButton.button == sf::Mouse::Left) {
						int ax = event.mouseButton.x / (2 * dotRad);
						int ay = event.mouseButton.y / (2 * dotRad);
						int type = board.dots[y + 2][x + 2].type;
						board.dots[y + 2][x + 2].type = board.dots[ay + 2][ax + 2].type;
						board.dots[ay + 2][ax + 2].type = type;
						pressed = false;
					}
				}

			}
		}


		board.update();

		window.clear(sf::Color::Black);

		board.draw(window);

		window.display();
	}

	return 0;
}